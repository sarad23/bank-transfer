FROM node:11
MAINTAINER baidyasharad@gmail.com

# Create app directory
WORKDIR /usr/src/app

# Copy the packages
COPY package.json ./

# Install the fresh dependencies
RUN npm install
COPY . .

EXPOSE 3000

# RUN npm run migrate_up
# RUN npm run populate

CMD ["npm", "start"]