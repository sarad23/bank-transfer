class Transaction{
    constructor(id, account, amount, type, created_date){
        this.id = id;
        this.account = account;
        this.amount = amount;
        this.transaction_type = type
        this.created_date = created_date;
    }
    static async getLatestOne(account){
        let query = Transaction.query;
        let result = await query("SELECT * FROM transaction WHERE account_id = ? ORDER BY id DESC LIMIT 1", [account.id]) ;
        if (result.length == 0) {
            return ;
        }
        else{
            let arr = [result[0].id, result[0].account_id, result[0].amount, result[0].transaction_type, result[0].created];
            return new Transaction(...arr);
        }
    }

}

module.exports = function(query){
    Transaction.query = query;
    return Transaction;
};