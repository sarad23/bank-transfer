const moment = require('moment');
const Transaction = require('./transaction');
const Err = require('../../lib/errors');

class Account{
    constructor(id, account_no, balance){
        this.id = id;
        this.account_no = account_no;
        this.balance = balance;
    }

    static async getAccounts(senderAccount, receiverAccount){
        let query = Account.query;
        let account = await query("select s.id as s_id, s.account_no as s_account ,s.balance as s_balance, r.id as r_id, r.balance as r_balance, r.account_no as r_account \
                                   from account as s , account as r where s.account_no = ? and r.account_no = ? for update", [senderAccount, receiverAccount]);
        if (account.length == 0 ) {
                // check which account doesn't  exists 
                // throw new Err.AccountNotFoundError({'message': `Account ${account_no}  doesn't exist`});
                let sender = await query("select * from account where account_no = ?", [senderAccount])
                if (sender.length == 0){
                    throw new Err.AccountNotFoundError({'message': `Sender Account ${senderAccount}  doesn't exist`});
                }
                let receiver = await query("select * from account where account_no = ?", [receiverAccount])
                if (receiver.length == 0){
                    throw new Err.AccountNotFoundError({'message': `Receiver Account ${receiverAccount}  doesn't exist`});
                }
                throw new Err.AccountNotFoundError({'message': `Account doesn't exist`});
        }
        let senderArr = [account[0].s_id,  account[0].s_account, account[0].s_balance,]
        let receiverArr = [account[0].r_id,  account[0].r_account, account[0].r_balance]
        return {
            "sender": new Account(...senderArr),
            "receiver": new Account(...receiverArr)
        }
    }

    static async getAll() {
        let query = Account.query;
        let results = await query("SELECT * FROM account");
        console.log(results);
        let accounts = [];
        for (let i in results) {
            accounts.push(new Account(results[i].id, results[i].account_no, results[i].balance));
        }
        return accounts;
    }

    static async transfer(fromAccount, toAccount, amount){
        let query = Account.query;
        try{
            await query("SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ")
            await query("START TRANSACTION");

            let {sender, receiver} = await Account.getAccounts(fromAccount, toAccount)
            
            await sender.checkBalance(amount);
            await sender.checkAccidentClick();
            
            sender.balance = sender.balance - amount;
            receiver.balance = receiver.balance + amount;
            // await query("SELECT SLEEP(10)");
            await query("UPDATE account SET balance=? WHERE id=?", [sender.balance , sender.id]);
            await query("UPDATE account SET balance=? WHERE id=?", [receiver.balance , receiver.id]);
            
            await query("INSERT INTO transaction (account_id, amount, transaction_type) VALUES (?, ?, ?)", [sender.id, amount, 'outgoing']);
            await query("INSERT INTO transaction (account_id, amount, transaction_type) VALUES (?, ?, ?)", [receiver.id, amount, 'incoming']);
            
            let transaction = await Transaction(query).getLatestOne(sender);
            
            await query("COMMIT");
            
            return {"sender": sender, "receiver": receiver, "transaction": transaction};
        }    
        catch (err) {
            await query("ROLLBACK");
            throw err;
        } 
    }

    async checkBalance(amount){
        if(amount > this.balance){
            throw new Err.InsufficentBalanceError({'message': 'Balance not sufficient to transfer'});
        } 
    }

    async checkAccidentClick(){
        let query = Account.query;
        let result = await Transaction(query).getLatestOne(this);
        if (result == null) {
           return 
        }
        
        let lastTransTime = moment(result.created_date, 'YYYY-MM-DD HH:mm:ss')
        if (moment().diff(lastTransTime) < 10000){
            throw new Err.FrequencyLimitExcededError({'message': 'Transfer rate limit exceeded. Frequency is 1 request per 10 sec'});
        }
    }
}

module.exports = function(query) { 
    Account.query = query;
    return Account; 
}

