const util = require('util')
const db = require('../conn')

const accounts = [
    {
        "account_no": "ABC001",
        "balance": 10000
    },
    {
        "account_no": "ABC002",
        "balance": 15000
    },
    {
        "account_no": "ABC003",
        "balance": 25000
    },
    {
        "account_no": "ABC004",
        "balance": 35000
    },

]

async function Seeder(){
    let conn = await db.driver();
    let query = util.promisify(conn.query).bind(conn);
    try{
        for (let index in accounts) {
            await query("INSERT INTO account (account_no, balance) VALUES (?, ?)", [ accounts[index]['account_no'], accounts[index]['balance'] ]);
        }
    }
    catch(err){
        console.log("Ran into some error")
    }
    finally{
        await conn.release();
        await conn.destroy();
    }
}

Seeder().then(function(){
    console.log("Seeded Successfully");
}).catch(function(err){
    console.log("Some Error");
});