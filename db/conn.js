const mysql = require('mysql');
const dbConfig = {
    user: "root",
    password: "root",
    database: "bank",
    host: "db",
    connectionLimit: 10,
    timezone: 'utc'
}

const util = require('util');
const pool = mysql.createPool(dbConfig);
const getConn = util.promisify(pool.getConnection).bind(pool);

async function getDBDriver(){
    let conn = await getConn();
    return conn;
}

module.exports.driver = getDBDriver