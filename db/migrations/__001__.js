const util = require('util');
const db = require('../conn');

const createAccount = `
            CREATE TABLE IF NOT EXISTS account(
                id INT AUTO_INCREMENT, 
                account_no VARCHAR(255) NOT NULL UNIQUE, 
                balance BIGINT(20) UNSIGNED NOT NULL DEFAULT 0, 
                PRIMARY KEY (id)
            )`;
const createTransaction = `
            CREATE TABLE IF NOT EXISTS transaction(
                id INT AUTO_INCREMENT,
                account_id INT NOT NULL, 
                amount bigint(20) UNSIGNED NOT NULL DEFAULT 0,
                transaction_type VARCHAR(16) NOT NULL, 
                created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
                
                PRIMARY KEY (id), 
                FOREIGN KEY (account_id) REFERENCES account (id) on delete cascade
            )`; 

async function Up(){
    let conn = await db.driver();
    let query = util.promisify(conn.query).bind(conn);
    await query(createAccount);
    await query(createTransaction);
    await conn.release();
    await conn.destroy();
}

async function Down(){
    let conn = await db.driver();
    let query = util.promisify(conn.query).bind(conn);
    await query("DROP TABLE transaction");
    await query("DROP TABLE account");
    await conn.release();
    await conn.destroy();
}   

const arg = process.argv
if (arg[2] == 'up') {
    Up().then(function(){
        console.log("Migrated Successfully");
    
    }).catch(function(err){
        console.log(err);
        console.log("Couldn't migarte it");
    })
}
else if(arg[2] == 'down'){
    Down().then(function(){
        console.log("Dropped Successfully");
    }).catch(function(err){
        console.log("Couldn't drop it");
        console.log(err)
    })
} 