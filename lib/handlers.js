const util = require('util');

const Account =  require('../db/models/account');
const Db = require('../db/conn');
const helpers = require('./helpers');

const getAllAccount = async (req, res) => {
    let data;
    let statusCode;
    try {
		var conn = await Db.driver();
        let query = util.promisify(conn.query).bind(conn);
        data = await Account(query).getAll();
    }
    catch(err) {
		statusCode = helpers.handleError(err);
        data = err.message;
    }
    finally{
		await conn.release();
        await conn.destroy();
        helpers.prepareResponse(res, data, statusCode);
	}
} 

const transfer = async (req, res) => {
    let data;
    let statusCode;
    try {
        var conn = await Db.driver();
		let query = util.promisify(conn.query).bind(conn);
        await helpers.validateInput(req.body);
        data = await Account(query).transfer(req.body.from, req.body.to, parseInt(req.body.amount));
	}
	catch(err) {
        statusCode = helpers.handleError(err);
        console.log(err.name);
        console.log(err.message);
		data = err.message;
	}
	finally{
		await conn.release();
        await conn.destroy();
        helpers.prepareResponse(res, data, statusCode);
	}
}

module.exports = {
    getAllAccount,
    transfer
}