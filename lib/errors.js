class AccountNotFoundError extends Error {
    constructor(message) {
        super(message)
        this.name = 'AccountNotFoundError';
        this.message = message;
    }
}

class InsufficentBalanceError extends Error {
    constructor(message) {
        super(message);
        this.name = 'InsufficentBalanceError';
        this.message = message;
    }
}

class FrequencyLimitExcededError extends Error {
    constructor(message) {
        super(message)
        this.name = 'FrequencyLimitExcededError';
        this.message = message;
    }
}

class ValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = 'ValidationError';
        this.message = message;
    }

}

module.exports  = { 
    AccountNotFoundError, 
    InsufficentBalanceError, 
    FrequencyLimitExcededError, 
    ValidationError 
};