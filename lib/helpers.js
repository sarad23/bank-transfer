const v = require('node-input-validator');

const Err = require('./errors');

const handleError = (err) => {
    let statusCode;
    switch(err.name) {
        case 'AccountNotFoundError':
            statusCode = 400
            break;
        case 'InsufficentBalanceError':
            statusCode = 400
            break;
        case 'FrequencyLimitExcededError':
            statusCode = 400
            break;
        case 'ValidationError':
            statusCode = 422
            break;
        default:
            statusCode = 500
    }
    return statusCode; 
}

v.customMessages({
    'from.different': 'Cannot transfer to the same account'
});

v.extend('different', async function (field, value, args) {
    if (value == this.inputs[args]){
        return false;
    }
    return true;
 
});


const validateInput = async (body) => {
    let validator = new v(body, {
        from:'required|different:to',
        to: 'required',
        amount: 'required|integer|min:1000'
    });

    let matched = await validator.check();
    if (!matched) {
        throw new Err.ValidationError(validator.errors);
    }
    // check source and destination same account
    return "success"; 
}

const prepareResponse = (res, payload, status=200) => {
    let data = {};
    if (status == 200) {
        data['status'] = 'success'
    }
    else{
        data['status'] = 'fail'
    }
    data['data'] = payload;
    res.writeHead(status, {"Content-Type": "application/json"});
    res.end(JSON.stringify(data));
}  

module.exports = {
    handleError,
    validateInput,
    prepareResponse

}