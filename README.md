# Transactions at the bank

This project transfer amount from one account to another. I have tried to keep the dependencies as much as minimal. Used the node http for web server and mysql for database.
Please do consider the fact that javascript is not my primary language.


## Database Structure
The database contains two tables account and transaction
```
Table account
   - id
   - account_no
   - balance

Table transaction
    - id
    - account_id
    - amount
    - transaction_type
    - created_date
    
```


## Run the application

To run the application u jus need to execute ./deploy.sh. 
The system initially contains dummy data. 
```
chmod +x deploy.sh
./deploy.sh
```
Wait untill the script completely executes. Then, go to http://localhost:3000. 
The system is deployed on docker. Big fan of docker, but not of a docker-compose.

## API description

For API description upload the postman_collection.json to postman. 
I mostly used JSend format (https://github.com/omniti-labs/jsend) for JSON response.
1. GET localhost:3000 => To get all the accounts.
2. POST localhost:3000/transfer => To transfer from one account to another. The request data is same as u specified however, i have changed the response format beacuse i have included the transaction date as well. 
It has all the data u specified as well as the description of transaction like date and amount as well.Below is the example of response.
```
{
        "sender": {
            "account_no": "ABC001",
            "balance": 8000
        },
        "receiver": {
            "account_no": "ABC002",
            "balance": 17000
        },
        "transaction": {
            "account": 1,
            "amount": 2000,
            "created_date": "2019-07-25T17:27:11.000Z"
        }
    }
```

## Consideration

###### What happens under high concurrency ?
I have used the isolation level REPEATABLE READ which is by default level of mysql.
This level avoids uncommitted data which is only commited data are visible to the system.
As well as there is a write lock imposed by a particular transaction on rows. So that other transaction cannot write to the same row. 
The lock is released when the transaction is ethier committed or rolledback. 
So, under high concurrency if it deals with different then it can work concurrenlty else it has to wait for transaction to complete    

###### What happens if your database becomes unavailable in the middle of your logic ?
Every write is executed by transaction. So, whenever there is connection loss in the middle of the transasction, 
it is never commited always rolled back. This is the default nature of mysql.This behaviuor is shown in this assignment.
   A better solution of this would using High Availability Mysql so that this problem never arises. 
Still if we consider there will be connection loss. Then we should implement queueing mechanism like rabbitMq etc, 
to hold the request and send those requests when database is up. To implement this it will go beyond the scope of this assignment.

###### What happens if 2 users (A, B) transfer money to user C at the same time ?
When this happens, under default settngs of mysql, isolation anomalies like "Lost Update" occur.
Both transaction reads from the same row at same time, there is no lock while reading
and then needs to updates the same read row which is done one after another.
While doing this the update of one transaction is overwritten by other.
To avoid this we simply need to impose the read lock like "SELECT ....FOR UPDATE". 
When the transaction executes this statement it imposes read lock and other transaction can
read only other the row has been updated and committed by lock imposer transaction. 
By doing this both of the updates are visible.

###### How accidental double click on transfer are handled ?
I have imposed trasaction frequency rule in the assignment. One can execute only one transction per 10 seconds.
He is blocked for 10 seconds after performing a transaction. This solves the accidental double clicks
   
## Note

The assignment is mostly contains es5 and some features of es6. 


