const connect = require('connect');
const finalhandler = require('finalhandler');
const http = require('http');
const Router = require('router');
const morgan = require('morgan');
const bodyParser = require('body-parser');


const handlers = require('./lib/handlers');

let route = Router();
let app = connect();

// port for listeining the server
const PORT = 3000;

route.get('/', handlers.getAllAccount);
route.post('/transfer', handlers.transfer);

app.use(bodyParser.json())
app.use(morgan('combined'))
app.use((req, res, next) => {
    route(req, res, finalhandler(req, res))
})

const server = http.createServer(app);
server.listen(PORT);