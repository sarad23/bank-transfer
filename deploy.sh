#!/usr/bin/env bash

# stop the containers
docker stop db
docker stop app

# remove the stooped continers
docker rm db
docker rm app

# run the dependent conatiner 
docker run --name db -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=bank -d  -p 3306:3306 mysql:5

# mysql needs some time to build
sleep 5

# build the clean app image with no cache
docker build --no-cache -t app .

# run the app
docker run --name app --link db:db -d -p 3000:3000 app 

# migrate and populate the app
docker exec -it app npm run migrate_up
docker exec -it app npm run populate

